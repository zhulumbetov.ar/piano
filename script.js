function playAudioAndHighlight(keyElement) {
    var key = keyElement.getAttribute('data-note');
    var audio = document.getElementById(key);
    audio.currentTime = 0;
    audio.play();
    
    keyElement.style.transform = 'scale(0.9)';
    keyElement.style.boxShadow = '0 0 10px yellow';
    keyElement.style.backgroundColor = '#fffff2';

    
  
    setTimeout(function() {
      keyElement.style.transform = '';
      keyElement.style.backgroundColor = '';
      keyElement.style.boxShadow = '';
    }, 300);
    
    if (keyElement.classList.contains('black-key')) {
      keyElement.style.backgroundColor = 'black';
      keyElement.style.boxShadow = '0 0 10px white';
    }
  }
  
  var keys = document.querySelectorAll('.white-key, .black-key');
  
  keys.forEach(function(key) {
    key.addEventListener('click', function() {
      playAudioAndHighlight(this);
    });
  });
  
  document.addEventListener('keydown', function(event) {
    var keyElement = document.querySelector('[data-note="' + event.key.toUpperCase() + '"]');
    if (keyElement) {
      playAudioAndHighlight(keyElement);
    }
  });
  